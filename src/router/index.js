import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import CategoryShow from '@/components/category/show'
import Categories from '@/components/category/Categories'
import Softwares from '@/components/softwares/Softwares'
import SoftwaresShow from '@/components/softwares/show'
import SoftwaresEdit from '@/components/softwares/edit'
import SoftwaresCreate from '@/components/softwares/create'



const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path:'/categories',
    name:'categories',
    component: Categories,
  },
  {
    path:'/category/show/:id',
    name:'categoryShow',
    component: CategoryShow,
  },
  {
    path:'/softwares',
    name:'softwares',
    component: Softwares,
  },
  {
    path:'/software/show/:id',
    name:'softwareShow',
    component: SoftwaresShow,
  },
  {
    path:'/software/edit/:id',
    name:'softwareEdit',
    component: SoftwaresEdit,
  },
  {
    path:'/softwares/create',
    name:'softwareCreate',
    component: SoftwaresCreate,
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
